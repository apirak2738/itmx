package services

import (
	"itmx/models"
	"itmx/repository"
)

type Services interface {

	// // Customer
	SvCustomerCreate(*models.CustomerCreateRequest) (*models.CustomerCreateResponse, error)
	SvCustomerAll() ([]models.CustomerModel, error)
	SvCustomerByID(id uint) (*models.CustomerByIDResponse, error)
	SvCustomerDeleted(id uint) error
	SvCustomerUpdated(*models.CustomerUpdated) (*models.CustomerUpdated, error)
}

type services struct {
	repo repository.Repository
}

func NewService(repo repository.Repository) services {
	return services{repo: repo}
}
