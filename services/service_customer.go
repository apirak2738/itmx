package services

import (
	"itmx/models"
	"itmx/utility"
)

func (sv services) SvCustomerCreate(req *models.CustomerCreateRequest) (*models.CustomerCreateResponse, error) {

	customerModel := &models.CustomerModel{
		Name: req.Name,
		Age:  req.Age,
	}

	customerCreate, err := sv.repo.RPCustomerCreate(customerModel)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return nil, err
	}

	res := models.CustomerCreateResponse{
		ID:   customerCreate.ID,
		Name: customerCreate.Name,
		Age:  customerCreate.Age,
	}

	return &res, nil
}

func (s services) SvCustomerAll() ([]models.CustomerModel, error) {

	data, err := s.repo.RPCustomerGetAll()
	if err != nil {
		utility.LogError.Println("err : ", err)
		return nil, err
	}

	return data, nil
}

func (s services) SvCustomerByID(id uint) (res *models.CustomerByIDResponse, err error) {

	response, err := s.repo.RPCustomerByID(id)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return res, err
	}

	return &models.CustomerByIDResponse{
		ID:   response.ID,
		Name: response.Name,
		Age:  response.Age,
	}, nil
}

func (s services) SvCustomerUpdated(req *models.CustomerUpdated) (*models.CustomerUpdated, error) {

	mode := &models.CustomerModel{
		ID:   req.ID,
		Name: req.Name,
		Age:  req.Age,
	}

	err := s.repo.RPCustomerUpdated(mode)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return nil, err
	}

	res := req

	return res, err
}

func (s services) SvCustomerDeleted(id uint) error {

	err := s.repo.RPCustomerDeleted(id)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return err
	}

	return err
}
