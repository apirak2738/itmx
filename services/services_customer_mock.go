package services

import (
	"itmx/constant"
	"itmx/models"

	"github.com/stretchr/testify/mock"
)

type customerServiceMock struct {
	mock.Mock
}

func NewCustomerServiceMock() *customerServiceMock {
	return &customerServiceMock{}
}

func (m *customerServiceMock) SvCustomerCreate(req *models.CustomerCreateRequest) (*models.CustomerCreateResponse, error) {

	if req.Name == "" || req.Age == 0 {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	res := models.CustomerCreateResponse{
		Name: req.Name,
		Age:  req.Age,
	}

	return &res, nil
}

func (m *customerServiceMock) SvCustomerByID(id uint) (*models.CustomerByIDResponse, error) {

	if id <= 0 {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	// ทำการบันทึกการเรียกใช้งานฟังก์ชันใน mock
	args := m.Called(id)

	// ดึงค่าที่ต้องการจากการเรียกใช้งานฟังก์ชันใน mock
	res := args.Get(0).(models.CustomerByIDResponse)
	err := args.Error(1)

	if id != res.ID {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return &res, err
}

func (m *customerServiceMock) SvCustomerUpdated(req *models.CustomerUpdated) (*models.CustomerUpdated, error) {

	if req.ID == 999 {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	if req.Name == "" || req.Age == 0 {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return req, nil
}

func (m *customerServiceMock) SvCustomerDeleted(id uint) error {

	if id <= 0 {
		return constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return nil
}
