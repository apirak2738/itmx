package main

import (
	"context"
	"fmt"
	"itmx/db"
	"itmx/handler"
	"itmx/repository"
	"itmx/services"
	"itmx/utility"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	config "github.com/spf13/viper"
)

func main() {

	e := echo.New()
	e.Use(middleware.Recover())

	db := db.ConnectDatabaseSqlite()
	repositoryDB := repository.NewRepository(db)
	services := services.NewService(repositoryDB)
	handler := handler.NewHandler(services)

	e.POST("customer", handler.CustomerCreate)
	e.GET("customer/:id", handler.CustomerByID)
	e.GET("customer/all", handler.CustomerAll)
	e.PUT("customer", handler.CustomerUpdated)
	e.DELETE("customer/:id", handler.CustomerDeleted)

	// Start server
	go func() {
		if err := e.Start(fmt.Sprintf(":%v", config.GetInt("app.port"))); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}

}

func init() {
	config.SetConfigFile("config.yml")
	if err := config.ReadInConfig(); err != nil {
		utility.LogError.Println("Fatal error env config (file): %v", err)
	}
}
