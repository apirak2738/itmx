package handler

import (
	"itmx/models"
	"itmx/utility"
	"strconv"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

var validate = validator.New()

func (h handler) CustomerCreate(c echo.Context) error {

	req := models.CustomerCreateRequest{}

	if err := c.Bind(&req); err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	// ทำการ validate
	if err := validate.Struct(req); err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	customerCreate, err := h.sv.SvCustomerCreate(&req)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	return utility.ResponseMessage(c, customerCreate, err)
}

func (h handler) CustomerAll(c echo.Context) error {

	customerByID, err := h.sv.SvCustomerAll()
	if err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}
	return utility.ResponseMessage(c, customerByID, err)
}

func (h handler) CustomerByID(c echo.Context) error {

	id, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	customerByID, err := h.sv.SvCustomerByID(uint(id))
	if err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}
	return utility.ResponseMessage(c, customerByID, err)
}

func (h handler) CustomerUpdated(c echo.Context) error {

	req := models.CustomerUpdated{}

	if err := c.Bind(&req); err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	// ทำการ validate
	if err := validate.Struct(req); err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	customerUpdated, err := h.sv.SvCustomerUpdated(&req)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	return utility.ResponseMessage(c, customerUpdated, err)
}

func (h handler) CustomerDeleted(c echo.Context) error {

	id, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	err = h.sv.SvCustomerDeleted(uint(id))
	if err != nil {
		utility.LogError.Println("err : ", err)
		return utility.ResponseMessage(c, nil, err)
	}

	return utility.ResponseMessage(c, []string{}, err)
}
