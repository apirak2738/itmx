package handler

import "itmx/services"

type handler struct {
	sv services.Services
}

func NewHandler(sv services.Services) handler {
	return handler{sv: sv}
}
