package repository

import (
	"itmx/models"

	"gorm.io/gorm"
)

type Repository interface {

	// Customer
	RPCustomerCreate(*models.CustomerModel) (*models.CustomerModel, error)
	RPCustomerGetAll() ([]models.CustomerModel, error)
	RPCustomerByID(id uint) (*models.CustomerModel, error)
	RPCustomerUpdated(*models.CustomerModel) error
	RPCustomerDeleted(id uint) error
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) repository {
	return repository{db: db}
}
