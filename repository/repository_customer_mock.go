package repository

import (
	"itmx/constant"
	"itmx/models"

	"github.com/stretchr/testify/mock"
)

type CustomerRepositoryMock struct {
	mock.Mock
}

func NewCustomerRepositoryMock() *CustomerRepositoryMock {
	return &CustomerRepositoryMock{}
}

func (cus *CustomerRepositoryMock) RPCustomerCreate(req *models.CustomerModel) (*models.CustomerModel, error) {

	if req.Name == "" || req.Age == 0 {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return req, nil
}

func (m *CustomerRepositoryMock) RPCustomerByID(id uint) (*models.CustomerModel, error) {

	if id <= 0 {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	// ทำการบันทึกการเรียกใช้งานฟังก์ชันใน mock
	args := m.Called(id)

	// ดึงค่าที่ต้องการจากการเรียกใช้งานฟังก์ชันใน mock
	res := args.Get(0).(models.CustomerModel)
	err := args.Error(1)

	if id != res.ID {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return &res, err
}

func (cus *CustomerRepositoryMock) RPCustomerUpdated(model *models.CustomerModel) error {

	if model.ID <= 0 {
		return constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	if model.Name == "" || model.Age == 0 {
		return constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return nil
}

func (cus *CustomerRepositoryMock) RPCustomerDeleted(id uint) error {

	if id <= 0 {
		return constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return nil
}
