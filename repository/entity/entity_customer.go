package entity

import "itmx/models"

type CustomerEntity struct {
	ID   uint `gorm:"primaryKey"`
	Name string
	Age  uint
}

func (CustomerEntity) TableName() string {
	return "customers"
}

func (CustomerEntity) ConvertModelsToEntity(models *models.CustomerModel) CustomerEntity {
	return CustomerEntity{
		ID:   models.ID,
		Name: models.Name,
		Age:  models.Age,
	}
}

func (CustomerEntity) ConvertEntityToModels(entity CustomerEntity) models.CustomerModel {
	return models.CustomerModel{
		ID:   entity.ID,
		Name: entity.Name,
		Age:  entity.Age,
	}
}
