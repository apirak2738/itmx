package repository

import (
	"errors"
	"itmx/constant"
	"itmx/models"
	"itmx/repository/entity"

	"gorm.io/gorm"
)

func (repo repository) RPCustomerCreate(req *models.CustomerModel) (*models.CustomerModel, error) {

	customerEntity := entity.CustomerEntity{}.ConvertModelsToEntity(req)

	tx := repo.db.Save(&customerEntity)
	if tx.Error != nil {
		return nil, tx.Error
	}

	if tx.RowsAffected == 0 {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	res := entity.CustomerEntity{}.ConvertEntityToModels(customerEntity)

	return &res, nil
}

func (repo repository) RPCustomerByID(id uint) (*models.CustomerModel, error) {

	customerEntity := entity.CustomerEntity{}

	tx := repo.db.Take(&customerEntity, id)
	if tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
		}
		return nil, tx.Error
	}

	res := entity.CustomerEntity{}.ConvertEntityToModels(customerEntity)

	return &res, nil
}

func (repo repository) RPCustomerUpdated(req *models.CustomerModel) error {

	customerEntity := entity.CustomerEntity{}

	tx := repo.db.Model(&customerEntity).Where("id = ?", req.ID).Updates(entity.CustomerEntity{Name: req.Name, Age: req.Age})
	if tx.Error != nil {
		return tx.Error
	}

	if tx.RowsAffected == 0 {
		return constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return nil
}

func (repo repository) RPCustomerDeleted(id uint) error {

	customerEntity := entity.CustomerEntity{}

	tx := repo.db.Delete(&customerEntity, id)
	if tx.Error != nil {
		return tx.Error
	}

	if tx.RowsAffected == 0 {
		return constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	return nil
}

func (repo repository) RPCustomerGetAll() (res []models.CustomerModel, err error) {

	et := []entity.CustomerEntity{}

	tx := repo.db.Find(&et)
	if tx.Error != nil {
		return nil, constant.MESSAGE_ERROR_DATA_NOT_FOUND
	}

	for _, value := range et {
		res = append(res, entity.CustomerEntity{}.ConvertEntityToModels(value))
	}

	return res, nil
}
