package utility

import (
	"log"
	"os"
)

var (
	LogInfo  = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	LogError = log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
)
