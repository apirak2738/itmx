package utility

import (
	"itmx/constant"
	"itmx/models"
	"net/http"

	"github.com/labstack/echo/v4"
)

func ResponseMessage(c echo.Context, data interface{}, err error) error {

	res := models.ResponseModel{}

	if err == nil {
		res.Message = constant.MESSAGE_SUCCESS
		res.Data = data
		return c.JSON(http.StatusOK, res)
	} else if err == constant.MESSAGE_ERROR_DATA_NOT_FOUND {
		res.Message = err.Error()
		res.Data = []string{}
		return c.JSON(http.StatusNotFound, res)
	} else {
		res.Message = err.Error()
		res.Data = []string{}
		return c.JSON(http.StatusInternalServerError, res)
	}
}
