
# ITMX
Create CRUD REST Application With Golang language

## Table of Contents

 - [Installation Steps](#installation-steps)
 - [Fromework](#fromework)
 - [Database](#database)
 -  [API Listing](#api-listing)
 - [How To Run](#how-to-run)
 - [How To Unit Test](#how-to-unit-test)
 - [Developer](#developer)

## Installation Steps
- https://medium.com/odds-team/%E0%B8%AA%E0%B8%A3%E0%B8%B8%E0%B8%9B%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%A3%E0%B8%B5%E0%B8%A2%E0%B8%99%E0%B8%9E%E0%B8%B7%E0%B9%89%E0%B8%99%E0%B8%90%E0%B8%B2%E0%B8%99%E0%B8%A0%E0%B8%B2%E0%B8%A9%E0%B8%B2-go-%E0%B9%81%E0%B8%9A%E0%B8%9A-step-by-step-%E0%B8%88%E0%B8%B2%E0%B8%81-course-pre-ultimate-go-by-p-yod-%E0%B8%95%E0%B8%AD%E0%B8%99-1-%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87-%E0%B9%81%E0%B8%A5%E0%B8%B0-d9ac7913e9a4


## Fromework

```
Golang
Echo
Gorm
```
## Database
```
SQLite
```

## API Listing


|`API`		       | `Method`       | `Endpoint`       |
| ------------      | ------------ | ---------------|
| CustomerCreate  | POST       | customer     |
| CustomerByID    | GET      | customer/:id     |
| CustomerUpdated | PUT       | customer     |
| CustomerDeleted | DELETE      | customer/:id     |


## How To Run

```
go run main.go
```

## How To Unit Test

```
go test ./... -v
```

## Developer
```
Apirak P.
```
  

  