package db

import (
	"itmx/repository/entity"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func ConnectDatabaseSqlite() *gorm.DB {

	// Open a connection to the SQLite database
	connectDB, err := gorm.Open(sqlite.Open("./db/sqlite/itmx.sqlite"), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	// AutoMigrate will create the 'users' table based on the User struct
	err = connectDB.AutoMigrate(&entity.CustomerEntity{})
	if err != nil {
		panic(err)
	}

	return connectDB
}
