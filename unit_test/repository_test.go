package unittest

import (
	"itmx/constant"
	"itmx/models"
	"itmx/repository"
	"itmx/services"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testCase struct {
	ID       uint
	Name     string
	Age      uint
	Expected string
}

func TestRepository(t *testing.T) {

	cases := []testCase{
		{ID: 1, Name: "Ton", Age: 25},
		{ID: 2, Name: "Win", Age: 26},
		{ID: 3, Name: "Tom", Age: 27},
		{ID: 4, Name: "Be", Age: 28},
	}

	// Create
	for _, c := range cases {

		t.Run("Success CustomerCreate", func(t *testing.T) {
			repositoryMockCreateData := repository.NewCustomerRepositoryMock()
			model := &models.CustomerCreateRequest{
				Name: c.Name,
				Age:  c.Age,
			}
			repositoryMockCreateData.On("RPCustomerCreate", model).Return(nil, nil)
			servicesCreateData := services.NewService(repositoryMockCreateData)
			_, err := servicesCreateData.SvCustomerCreate(model)
			assert.Equal(t, nil, err)
		})

		t.Run("Success CustomerByID", func(t *testing.T) {
			repositoryMockGetData := repository.NewCustomerRepositoryMock()
			repositoryMockGetData.On("RPCustomerByID", c.ID).Return(models.CustomerModel{
				ID:   c.ID,
				Name: c.Name,
				Age:  c.Age,
			}, nil)
			servicesGetData := services.NewService(repositoryMockGetData)
			_, err := servicesGetData.SvCustomerByID(c.ID)
			assert.Equal(t, nil, err)
		})

		t.Run("Success CustomerUpdate", func(t *testing.T) {
			repositoryMock := repository.NewCustomerRepositoryMock()
			model := &models.CustomerUpdated{
				ID:   c.ID,
				Name: c.Name,
				Age:  c.Age,
			}
			repositoryMock.On("RPCustomerUpdated", model).Return(nil, nil)
			services := services.NewService(repositoryMock)
			_, err := services.SvCustomerUpdated(model)
			assert.Equal(t, nil, err)
		})

		t.Run("Success CustomerDeleted", func(t *testing.T) {
			//Arrage
			repositoryMock := repository.NewCustomerRepositoryMock()

			repositoryMock.On("RPCustomerDeleted", c.ID).Return(nil, nil)
			services := services.NewService(repositoryMock)
			err := services.SvCustomerDeleted(c.ID)
			assert.Equal(t, nil, err)
		})
	}

	t.Run("Error CustomerCreate", func(t *testing.T) {
		repositoryMockCreateData := repository.NewCustomerRepositoryMock()
		model := &models.CustomerCreateRequest{
			Name: "",
			Age:  0,
		}
		repositoryMockCreateData.On("RPCustomerCreate", model).Return(nil, nil)
		servicesCreateData := services.NewService(repositoryMockCreateData)
		_, err := servicesCreateData.SvCustomerCreate(model)
		assert.Equal(t, constant.MESSAGE_ERROR_DATA_NOT_FOUND, err)
	})

	t.Run("Error CustomerByID", func(t *testing.T) {
		//Arrage
		repositoryMock := repository.NewCustomerRepositoryMock()
		repositoryMock.On("RPCustomerByID", uint(99)).Return(models.CustomerModel{
			ID:   2,
			Name: "Win",
			Age:  26,
		}, nil)
		services := services.NewService(repositoryMock)
		_, err := services.SvCustomerByID(uint(99))
		assert.Equal(t, constant.MESSAGE_ERROR_DATA_NOT_FOUND, err)
	})

	t.Run("Error CustomerUpdated", func(t *testing.T) {
		repositoryMockCreateData := repository.NewCustomerRepositoryMock()
		model := &models.CustomerUpdated{
			Name: "",
			Age:  0,
		}
		repositoryMockCreateData.On("RPCustomerUpdated", model).Return(nil, nil)
		servicesCreateData := services.NewService(repositoryMockCreateData)
		_, err := servicesCreateData.SvCustomerUpdated(model)
		assert.Equal(t, constant.MESSAGE_ERROR_DATA_NOT_FOUND, err)
	})

	t.Run("Error CustomerDeleted", func(t *testing.T) {
		repositoryMock := repository.NewCustomerRepositoryMock()

		repositoryMock.On("RPCustomerDeleted", 0).Return(nil, nil)
		services := services.NewService(repositoryMock)
		err := services.SvCustomerDeleted(0)
		assert.Equal(t, constant.MESSAGE_ERROR_DATA_NOT_FOUND, err)
	})

}
