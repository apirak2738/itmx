package unittest

import (
	"bytes"
	"encoding/json"
	"io"
	"itmx/handler"
	"itmx/models"
	"itmx/services"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestHandler(t *testing.T) {

	cases := []testCase{
		{ID: 1, Name: "Ton", Age: 25},
		{ID: 2, Name: "Win", Age: 26},
		{ID: 3, Name: "Tom", Age: 27},
		{ID: 4, Name: "Be", Age: 28},
	}

	// Create
	for _, c := range cases {

		t.Run("Success CustomerCreate", func(t *testing.T) {

			customerService := services.NewCustomerServiceMock()
			models := models.CustomerCreateRequest{
				Name: c.Name,
				Age:  c.Age,
			}
			customerService.On("SvCustomerCreate", models).Return(nil, nil)

			handler := handler.NewHandler(customerService)
			var buf bytes.Buffer
			_ = json.NewEncoder(&buf).Encode(models)

			e, rec := CallMothPost(&buf, http.MethodPost)

			handler.CustomerCreate(e)
			assert.Equal(t, http.StatusOK, rec.Code)
		})

		t.Run("Success CustomerByID", func(t *testing.T) {

			customerService := services.NewCustomerServiceMock()
			customerService.On("SvCustomerByID", uint(c.ID)).Return(models.CustomerByIDResponse{
				ID:   c.ID,
				Name: c.Name,
				Age:  c.Age,
			}, nil)

			handler := handler.NewHandler(customerService)
			id := strconv.FormatUint(uint64(c.ID), 10)
			e, rec := CallMothGet(id, http.MethodGet)

			handler.CustomerByID(e)
			assert.Equal(t, http.StatusOK, rec.Code)
		})

		t.Run("Success CustomerUpdated", func(t *testing.T) {

			customerService := services.NewCustomerServiceMock()
			models := models.CustomerUpdated{
				ID:   c.ID,
				Name: c.Name,
				Age:  c.Age,
			}
			customerService.On("SvCustomerUpdated", models).Return(nil, nil)

			handler := handler.NewHandler(customerService)
			var buf bytes.Buffer
			_ = json.NewEncoder(&buf).Encode(models)

			e, rec := CallMothPost(&buf, http.MethodPut)

			handler.CustomerUpdated(e)
			assert.Equal(t, http.StatusOK, rec.Code)
		})

		t.Run("Success CustomerDeleted", func(t *testing.T) {

			customerService := services.NewCustomerServiceMock()
			customerService.On("SvCustomerDeleted", uint(c.ID)).Return(nil, nil)

			handler := handler.NewHandler(customerService)
			id := strconv.FormatUint(uint64(c.ID), 10)
			e, rec := CallMothGet(id, http.MethodDelete)

			handler.CustomerDeleted(e)
			assert.Equal(t, http.StatusOK, rec.Code)
		})

	}

	t.Run("Error CustomerCreate", func(t *testing.T) {

		customerService := services.NewCustomerServiceMock()
		models := models.CustomerCreateRequest{
			Name: "",
			Age:  0,
		}
		customerService.On("SvCustomerCreate", models).Return(nil, nil)

		handler := handler.NewHandler(customerService)
		var buf bytes.Buffer
		_ = json.NewEncoder(&buf).Encode(models)

		e, rec := CallMothPost(&buf, http.MethodPost)

		handler.CustomerCreate(e)
		assert.Equal(t, http.StatusInternalServerError, rec.Code)
	})

	t.Run("Error CustomerByID", func(t *testing.T) {

		customerService := services.NewCustomerServiceMock()
		customerService.On("SvCustomerByID", uint(99)).Return(models.CustomerByIDResponse{
			ID:   1,
			Name: "Ton",
			Age:  25,
		}, nil)

		handler := handler.NewHandler(customerService)
		id := strconv.FormatUint(uint64(99), 10)
		e, rec := CallMothGet(id, http.MethodGet)

		handler.CustomerByID(e)
		assert.Equal(t, http.StatusNotFound, rec.Code)
	})

	t.Run("Error CustomerUpdated", func(t *testing.T) {

		customerService := services.NewCustomerServiceMock()
		models := models.CustomerUpdated{
			ID:   999,
			Name: "Ton",
			Age:  25,
		}
		customerService.On("SvCustomerUpdated", models).Return(nil, nil)

		handler := handler.NewHandler(customerService)
		var buf bytes.Buffer
		_ = json.NewEncoder(&buf).Encode(models)

		e, rec := CallMothPost(&buf, http.MethodPut)

		handler.CustomerUpdated(e)
		assert.Equal(t, http.StatusNotFound, rec.Code)
	})

	t.Run("Error CustomerDeleted", func(t *testing.T) {

		customerService := services.NewCustomerServiceMock()
		customerService.On("SvCustomerDeleted", uint(0)).Return(nil, nil)

		handler := handler.NewHandler(customerService)
		id := strconv.FormatUint(uint64(0), 10)
		e, rec := CallMothGet(id, http.MethodDelete)

		handler.CustomerDeleted(e)
		assert.Equal(t, http.StatusNotFound, rec.Code)
	})

}

func CallMothGet(id, method string) (echo.Context, *httptest.ResponseRecorder) {

	e := echo.New()
	req := httptest.NewRequest(method, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/customer/:id")
	c.SetParamNames("id")
	c.SetParamValues(id)
	return c, rec
}

func CallMothPost(requestBody io.Reader, method string) (echo.Context, *httptest.ResponseRecorder) {
	e := echo.New()
	req := httptest.NewRequest(method, "/", requestBody)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/customer")

	return c, rec
}
