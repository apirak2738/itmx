package constant

import "errors"

var (

	// Status Error
	MESSAGE_ERROR_DATA_NOT_FOUND = errors.New("data not found")

	// Status Succuss
	MESSAGE_SUCCESS string = "success"
)
