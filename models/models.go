package models

type ResponseModel struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type CustomerModel struct {
	ID   uint
	Name string
	Age  uint
}
