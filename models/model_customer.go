package models

type CustomerCreateRequest struct {
	Name string `json:"name" validate:"required"`
	Age  uint   `json:"age" validate:"required"`
}

type CustomerCreateResponse struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
	Age  uint   `json:"age"`
}

type CustomerByIDResponse struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
	Age  uint   `json:"age"`
}

type CustomerUpdated struct {
	ID   uint   `json:"id" validate:"required"`
	Name string `json:"name" validate:"required"`
	Age  uint   `json:"age" validate:"required"`
}
